package com.airport.mvvmsample.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;



public class MarginDimen {
    public static float getDimension(Context context, int margin) {
        Resources resources = context.getResources();

        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                margin,
                resources.getDisplayMetrics());
    }
}
