package com.airport.mvvmsample.util;

import android.content.Intent;
import android.os.Bundle;

import java.util.Collection;

public class AppUitl {

    public static boolean hasData(Collection<?> paramCollection) {
        return !isEmpty(paramCollection);
    }

    public static boolean isEmpty(Collection<?> paramCollection) {
        boolean bool = false;
        if ((paramCollection == null) || (paramCollection.isEmpty())) {
            bool = true;
        }
        return bool;
    }

    public static int getComingFrom(Intent paramIntent) {

        Bundle bundle = paramIntent.getExtras();

        if (bundle != null && bundle.containsKey("COMING_FROM")) {

            return bundle.getInt("COMING_FROM", 0);

        }
        return 0;
    }

    public static Object getIntentData(Intent paramIntent) {
        Object localObject = null;
        Bundle localBundle = paramIntent.getExtras();
        paramIntent = (Intent) localObject;
        if (localBundle != null) {
            paramIntent = (Intent) localObject;
            if (localBundle.containsKey("INTENT_KEY_DATA")) {
                paramIntent = localBundle.getParcelable("INTENT_KEY_DATA");
            }
        }
        return paramIntent;
    }


}
