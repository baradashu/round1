//package com.airport.mvvmsample.util;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//
//import com.google.gson.Gson;
//import com.unicorn.umart.app.UmartApp;
//import com.unicorn.umart.pojo.user.User;
//
///**
// * Created by Ashvini on 8/21/2018.
// */
//
//public class SharedPreferenceUtil {
//
//    public static final String UM_TOKEN = "UMTOKEN";
//    public static String LOGIN_DATA_KEY = "logindata";
//    public static String USER_ID = "userid";
//    public static String MOBILE_NO = "mobileno";
//    public static String SEARCH_HISTORY ="searchHistory";
//
//    public static User getSavedUserFromPreference(Class<User> userClass) {
//
//        SharedPreferences pref = UmartApp.getInstance()
//                .getSharedPreferences(LOGIN_DATA_KEY, Context.MODE_PRIVATE);
//        if (pref.contains(LOGIN_DATA_KEY)) {
//            return (User) new Gson().fromJson(pref.getString(LOGIN_DATA_KEY, ""), userClass);
//        }
//        return null;
//
//
//    }
//
//
//    public static void saveUserToSharedPreference(User user) {
//
//        SharedPreferences.Editor pref = UmartApp.getInstance()
//                .getSharedPreferences(LOGIN_DATA_KEY, Context.MODE_PRIVATE)
//                .edit();
//        pref.putString(LOGIN_DATA_KEY, new Gson().toJson(user));
//        pref.apply();
//
//
//    }
//
//
//    public static void saveUmTokenToSharedPreference(String umtoekn) {
//        SharedPreferences.Editor pref = UmartApp.getInstance()
//                .getSharedPreferences(UM_TOKEN, Context.MODE_PRIVATE)
//                .edit();
//        pref.putString(UM_TOKEN, umtoekn);
//        pref.apply();
//    }
//
//
//    public static String geUmTokenFromPreference() {
//        SharedPreferences pref = UmartApp.getInstance()
//                .getSharedPreferences(UM_TOKEN, Context.MODE_PRIVATE);
//
//        return pref.getString(UM_TOKEN, "umart");
//
//    }
//
//    public static void saveSaerchHistoryToSharedPreference(String searchHistory) {
//        SharedPreferences.Editor pref = UmartApp.getInstance()
//                .getSharedPreferences(SEARCH_HISTORY, Context.MODE_PRIVATE)
//                .edit();
//        pref.putString(SEARCH_HISTORY, searchHistory);
//        pref.apply();
//    }
//
//
//    public static String getSearchHistoryFromPreference() {
//        SharedPreferences pref = UmartApp.getInstance()
//                .getSharedPreferences(SEARCH_HISTORY, Context.MODE_PRIVATE);
//
//        return pref.getString(SEARCH_HISTORY, null);
//
//    }
//
//
//}
