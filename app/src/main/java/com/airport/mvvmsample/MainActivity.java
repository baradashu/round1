package com.airport.mvvmsample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.airport.mvvmsample.activities.BaseActivity;
import com.airport.mvvmsample.activities.BookApiActivity;
import com.airport.mvvmsample.activities.NewsListActivity;
import com.airport.mvvmsample.interfaces.Injectable;

import dagger.android.AndroidInjection;

public class MainActivity extends BaseActivity implements Injectable {

    public static String clicked = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Infodis Infotech");


    }

    public void onNewClick(View view) {
        MainActivity.clicked = "news";
        startActivity(new Intent(MainActivity.this, NewsListActivity.class));
    }

    public void onBookClick(View view) {
        MainActivity.clicked = "books";
        startActivity(new Intent(MainActivity.this, BookApiActivity.class));
    }

}
