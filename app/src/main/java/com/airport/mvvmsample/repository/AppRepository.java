package com.airport.mvvmsample.repository;



import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.airport.mvvmsample.common.AppExecutors;
import com.airport.mvvmsample.models.NewsItemListModel;
import com.airport.mvvmsample.web.ApiResponse;
import com.airport.mvvmsample.web.Resource;
import com.airport.mvvmsample.web.WebApi;


import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.airport.mvvmsample.models.BookItemListModel;



public class AppRepository {

    private WebApi webApi;

    private final AppExecutors executor;
    final MediatorLiveData productListResult;

    @Inject
    public AppRepository(WebApi webApi, AppExecutors executor) {
        this.webApi = webApi;
        this.executor = executor;
        productListResult = new MediatorLiveData<>();
    }



    public LiveData<Resource<NewsItemListModel>> getNewsList( String url) {

        final MediatorLiveData result = new MediatorLiveData<>();
        result.setValue(Resource.loading(new NewsItemListModel()));

        webApi.getNewsList(url).enqueue(new Callback<NewsItemListModel>() {
            @Override
            public void onResponse(Call<NewsItemListModel> call, Response<NewsItemListModel> response) {
                ApiResponse<NewsItemListModel> apiResponse = new ApiResponse<>(response);
                if (response.raw().cacheResponse() != null) {
                    // true: response was served from cache
                    result.setValue(Resource.success(response.body()));
                }

                if (response.raw().networkResponse() != null) {
                    // true: response was served from network/server
                    result.setValue(Resource.success(response.body()));
                }
            }

            @Override
            public void onFailure(Call<NewsItemListModel> call, Throwable t) {
                result.setValue(Resource.error(t.getMessage(), new NewsItemListModel()));
            }
        });


        return result;
    }

  public LiveData<Resource<BookItemListModel>> getBookList( String url) {

        final MediatorLiveData result = new MediatorLiveData<>();
        result.setValue(Resource.loading(new NewsItemListModel()));

        webApi.getBookList(url).enqueue(new Callback<BookItemListModel>() {
            @Override
            public void onResponse(Call<BookItemListModel> call, Response<BookItemListModel> response) {
                ApiResponse<BookItemListModel> apiResponse = new ApiResponse<>(response);
                if (response.raw().cacheResponse() != null) {
                    // true: response was served from cache
                    result.setValue(Resource.success(response.body()));
                }

                if (response.raw().networkResponse() != null) {
                    // true: response was served from network/server
                    result.setValue(Resource.success(response.body()));
                }
            }

            @Override
            public void onFailure(Call<BookItemListModel> call, Throwable t) {
                result.setValue(Resource.error(t.getMessage(), new BookItemListModel()));
            }
        });


        return result;
    }


}
