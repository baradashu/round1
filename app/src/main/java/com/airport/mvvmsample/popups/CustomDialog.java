package com.airport.mvvmsample.popups;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;

import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.airport.mvvmsample.R;
import com.airport.mvvmsample.ui.AnimatedGifImageView;




public class CustomDialog extends Dialog {
    public CustomDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        ((AnimatedGifImageView) findViewById(R.id.animatedGifImageView)).setAnimatedGif(R.drawable.loading, AnimatedGifImageView.TYPE.STREACH_TO_FIT);
    }
}
