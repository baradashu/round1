package com.airport.mvvmsample.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;

import com.airport.mvvmsample.R;
import com.airport.mvvmsample.activities.BookApiActivity;
import com.airport.mvvmsample.activities.NewsListActivity;
import com.airport.mvvmsample.common.DataBoundListAdapter;
import com.airport.mvvmsample.common.DataBoundViewHolder;
import com.airport.mvvmsample.databinding.BookItemBinding;
import com.airport.mvvmsample.databinding.NewsItemBinding;
import com.airport.mvvmsample.models.BookItem;
import com.airport.mvvmsample.models.NewsItem;

import java.util.Objects;
import java.util.Random;

public class BookAdaptor extends DataBoundListAdapter<BookItem, BookItemBinding> {

    private final BookApiActivity.BookItemClick bookItemClick;


    public BookAdaptor(BookApiActivity.BookItemClick bookItemClick) {
        this.bookItemClick = bookItemClick;
    }
    @Override
    protected BookItemBinding createBinding(ViewGroup parent) {
        final BookItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.book_item, parent, false);


        return binding;
    }

    @Override
    protected void bind(DataBoundViewHolder<BookItemBinding> holder, BookItem item, int position) {
        BookItemBinding bookItemBinding = holder.binding;
        bookItemBinding.setBookdata(item);
        bookItemBinding.setVolumndata(item.getVolumeInfo());
        bookItemBinding.setThumbnail(item.getVolumeInfo().getImageLinks().getThumbnail());

        bookItemBinding.imgbckview.setCardBackgroundColor(generateRandomColor());


        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookItemClick.onBookItemCLick(item, true);
            }
        });

    }


    final Random mRandom = new Random(System.currentTimeMillis());

    public int generateRandomColor() {
        // This is the base color which will be mixed with the generated one
        final int baseColor = Color.WHITE;

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) / 2;
        final int green = (baseGreen + mRandom.nextInt(256)) / 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) / 2;

        return Color.rgb(red, green, blue);
    }
    @Override
    protected boolean areItemsTheSame(BookItem oldItem, BookItem newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId());

    }

    @Override
    protected boolean areContentsTheSame(BookItem oldItem, BookItem newItem) {
        return false;
    }
}
