package com.airport.mvvmsample.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.airport.mvvmsample.R;
import com.airport.mvvmsample.activities.NewsListActivity;
import com.airport.mvvmsample.common.DataBoundListAdapter;
import com.airport.mvvmsample.common.DataBoundViewHolder;
import com.airport.mvvmsample.databinding.NewsItemBinding;
import com.airport.mvvmsample.models.NewsItem;
import com.kwabenaberko.newsapilib.models.Article;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.Objects;

public class NewsAdaptor extends DataBoundListAdapter<Article, NewsItemBinding> {

    private final NewsListActivity.NewsItemClick newsItemClick;


    public NewsAdaptor(NewsListActivity.NewsItemClick nwitemclick) {
        this.newsItemClick = nwitemclick;
    }
    @Override
    protected NewsItemBinding createBinding(ViewGroup parent) {
        final NewsItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.news_item, parent, false);


        return binding;
    }

    @Override
    protected void bind(DataBoundViewHolder<NewsItemBinding> holder, Article item, int position) {
        NewsItemBinding newsItemBinding = holder.binding;
        //newsItemBinding.setNewsdata(item);
        newsItemBinding.newstitle.setText(item.getTitle());
        String date = item.getPublishedAt();
        try {


            date = item.getPublishedAt().substring(0,19);


            newsItemBinding.newspublishat.setText(date.replace("T", " "));
        } catch (Exception e) {
            e.printStackTrace();
            newsItemBinding.newspublishat.setText(date);
        }
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(1)
                .cornerRadiusDp(10)

                .oval(false)
                .build();
        Picasso.get()
                .load(item.getUrlToImage())
                .fit()
                .transform(transformation)
                .placeholder(R.drawable.placeholder_news)
                .error(R.drawable.placeholder_news)
                .into(newsItemBinding.newsimg);

        holder.binding.lincont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newsItemClick.onNewItemCLick(item, true);
            }
        });

    }

    @Override
    protected boolean areItemsTheSame(Article oldItem, Article newItem) {
        return Objects.equals(oldItem.getTitle(), newItem.getTitle());

    }

    @Override
    protected boolean areContentsTheSame(Article oldItem, Article newItem) {
        return false;
    }
}
