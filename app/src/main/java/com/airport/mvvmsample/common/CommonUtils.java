package com.airport.mvvmsample.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class CommonUtils {
    public static boolean isNetworkAvailable(Context paramContext) {
        ConnectivityManager connectivity = (ConnectivityManager) paramContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();

            if ((networkInfo != null) && (networkInfo.getState() == NetworkInfo.State.CONNECTED)) {
                Log.w("INTERNET:", "connected!");
                return true;
            }
        }
        return false;
    }
}
