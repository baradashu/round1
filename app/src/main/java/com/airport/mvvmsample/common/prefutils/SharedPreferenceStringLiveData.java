package com.airport.mvvmsample.common.prefutils;

import android.content.SharedPreferences;



public final class SharedPreferenceStringLiveData extends SharedPreferenceLiveData<String> {


    public SharedPreferenceStringLiveData(SharedPreferences sharedPrefs, String key, String defValue) {
        super(sharedPrefs, key, defValue);
    }

    @Override
    public String getValueFromPreferences(String key, String defValue) {
        return getSharedPrefs().getString(key, defValue);
    }
}