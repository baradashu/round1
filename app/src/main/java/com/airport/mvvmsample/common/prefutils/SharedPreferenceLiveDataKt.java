package com.airport.mvvmsample.common.prefutils;

import android.content.SharedPreferences;



public class SharedPreferenceLiveDataKt {

    public static final SharedPreferenceLiveData stringLiveData(SharedPreferences receiver, String key, String defValue) {

        return (SharedPreferenceLiveData) (new SharedPreferenceStringLiveData(receiver, key, defValue));
    }

}
