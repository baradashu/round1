/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.airport.mvvmsample.componant;

import com.airport.mvvmsample.MainActivity;
import com.airport.mvvmsample.app.MvvmApp;
import com.airport.mvvmsample.common.CommonUtils;
import com.airport.mvvmsample.common.LiveDataCallAdapterFactory;
import com.airport.mvvmsample.web.WebApi;
//import com.unicorn.umart.app.UmartApp;
//import com.unicorn.umart.common.CommonUtils;
//import com.unicorn.umart.common.LiveDataCallAdapterFactory;
//import com.unicorn.umart.util.SharedPreferenceUtil;
//import com.unicorn.umart.web.WebApi;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@Module(includes = ViewModelModule.class)
class AppModule {



    @Singleton
    @Provides
    WebApi provideWebApi() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Interceptor headerInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {


                Request.Builder requesrBulder = chain.request().newBuilder();
               // String key = SharedPreferenceUtil.geUmTokenFromPreference();
               // requesrBulder.addHeader("um_token", "key");
                Response response = null;
                try {
                    response = chain.proceed(requesrBulder.build());
                } catch (IOException e) {
                    if (!CommonUtils.isNetworkAvailable(MvvmApp.getInstance())) {
                        throw new NoConnectivityException();
                    } else {
                        throw e;
                    }

                }


                return response;
            }
        };
        builder.addInterceptor(headerInterceptor);
        builder.cache(new Cache(MvvmApp.getInstance().getCacheDir(), 10 * 1024 * 1024));

        String url ="";
        if(MainActivity.clicked.equals("news")){
          url ="https://newsapi.org/v2/"  ;
        }else{
            url="https://www.googleapis.com/books/";
        }

        return new Retrofit.Builder()
                .client(builder.build())
                .baseUrl("https://www.googleapis.com/books/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(WebApi.class);
    }



   /* @Singleton
    @Provides
    StudentDatabase provideDb(Application app) {
        return Room.databaseBuilder(app, StudentDatabase.class, "student.db").build();
    }

    @Singleton
    @Provides
    StudentDao provideUserDao(StudentDatabase db) {
        return db.studentDao();
    }*/

}
