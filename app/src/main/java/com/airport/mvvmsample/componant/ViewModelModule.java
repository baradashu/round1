package com.airport.mvvmsample.componant;



import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.airport.mvvmsample.viewmodel.AppViewModel;
import com.airport.mvvmsample.viewmodel.AppViewModelFactory;


import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(AppViewModel.class)
    abstract  ViewModel bindAppViewModel(AppViewModel umartViewModel);


    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(AppViewModelFactory factory);



}
