package com.airport.mvvmsample.componant;

import java.io.IOException;

public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return "no internet";
    }
}
