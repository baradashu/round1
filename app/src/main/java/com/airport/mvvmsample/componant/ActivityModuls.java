package com.airport.mvvmsample.componant;


import com.airport.mvvmsample.MainActivity;
import com.airport.mvvmsample.activities.BaseActivity;
import com.airport.mvvmsample.activities.BookApiActivity;
import com.airport.mvvmsample.activities.BookDeatilsActivity;
import com.airport.mvvmsample.activities.NewsDetailsActivity;
import com.airport.mvvmsample.activities.NewsListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;



@Module
public abstract class ActivityModuls {

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();
//
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract BaseActivity contributeBaseActivity();
//
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract NewsListActivity contributeNewActivity();
//
//
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract NewsDetailsActivity contributeNewsDetailsActivity();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract BookApiActivity contributeBookAPI();

    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract BookDeatilsActivity contributeBookDetailsActivityAPI();

}
