package com.airport.mvvmsample.app;

import android.app.Activity;
import android.app.Application;


import com.airport.mvvmsample.R;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
//import dagger.android.HasActivityInjector;
import dagger.android.HasActivityInjector;
//import dagger.android.HasAndroidInjector;



public class MvvmApp extends Application implements HasActivityInjector {

    private static MvvmApp sInstance;
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;
//    @Inject
//    DispatchingAndroidInjector androidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        AppInjector.init(this);

    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    public static MvvmApp getInstance() {
        return sInstance;
    }

//    public static String getServerImagePath(String imagePath) {
//        imagePath = sInstance.getResources().getString(R.string.imageserver) + imagePath;
//        return imagePath;
//    }

//    @Override
//    public AndroidInjector<Object> androidInjector() {
//        return androidInjector;
//    }


//    @Override
//    public AndroidInjector<Object> androidInjector() {
//        return androidInjector;
//
//    }
}
