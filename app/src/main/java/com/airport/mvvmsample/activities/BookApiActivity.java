package com.airport.mvvmsample.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;
import com.airport.mvvmsample.models.BookItemListModel;
import com.airport.mvvmsample.R;
import com.airport.mvvmsample.adapters.BookAdaptor;
import com.airport.mvvmsample.adapters.NewsAdaptor;
import com.airport.mvvmsample.databinding.ActivityBookApiBinding;
import com.airport.mvvmsample.databinding.ActivityNewsListBinding;
import com.airport.mvvmsample.models.BookItem;
import com.airport.mvvmsample.models.NewsItem;
import com.airport.mvvmsample.models.NewsItemListModel;
import com.airport.mvvmsample.viewmodel.AppViewModel;
import com.airport.mvvmsample.web.Resource;

import java.util.ArrayList;
import java.util.List;

import dagger.android.AndroidInjection;

public class BookApiActivity extends BaseActivity {

    private AppViewModel viewModel;
    ActivityBookApiBinding binding;
    BookAdaptor adapter;
    List<BookItem> bookItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_api);
        binding = DataBindingUtil.bind(getRootView());
        AndroidInjection.inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AppViewModel.class);
        setTitle(getString(R.string.books));
        setHomeEnabled(true);
        bookItemList = new ArrayList<>();
        BookApiActivity.BookItemClick bookItemClick = new BookApiActivity.BookItemClick() {
            @Override
            public void onBookItemCLick(BookItem bookItem,boolean isClicked) {

                Intent intent = new Intent(BookApiActivity.this , BookDeatilsActivity.class);
                intent.putExtra("bookdata",bookItem);
                startActivity(intent);


            }
        };


        adapter = new BookAdaptor(bookItemClick);
        binding.bookList.setAdapter(adapter);
        adapter.replace(bookItemList);


        binding.edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String action_url = "volumes?q="+binding.edtSearch.getText().toString();

                    getBookList(action_url);
                    handled = true;
                }
                return handled;
            }
        });


        String action_url = "volumes?q=cancer";

        getBookList(action_url);
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//
//    }

    public void getBookList(String url) {
        viewModel.getBookList(url).observe(this, new Observer<Resource<BookItemListModel>>() {
            @Override
            public void onChanged(@Nullable Resource<BookItemListModel> bookItemListModelResource) {
                switch (bookItemListModelResource.status) {
                    case SUCCESS:
                        hideProgressDialog();
                        bookItemList = bookItemListModelResource.data.getItems();
                        if (bookItemList != null) {
                            adapter.replace(bookItemList);
                        }
                        break;
                    case ERROR:
                        hideProgressDialog();
                        Toast.makeText(BookApiActivity.this, "Error While fetching data", Toast.LENGTH_LONG).show();
                        break;
                    case LOADING:
                        showProgressDialog();
                        break;
                }

            }
        });
    }

    public  interface BookItemClick{
        void   onBookItemCLick(BookItem bookItem , boolean isclick);
    }
}