package com.airport.mvvmsample.activities;

import android.app.Dialog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.design.widget.AppBarLayout;
//import android.support.design.widget.CoordinatorLayout;
//import android.support.design.widget.Snackbar;
//import android.support.v4.app.Fragment;
//import android.support.v4.view.GravityCompat;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.airport.mvvmsample.MainActivity;
import com.airport.mvvmsample.R;
import com.airport.mvvmsample.fragments.HomeFragment;
import com.airport.mvvmsample.popups.CustomDialog;
import com.airport.mvvmsample.viewmodel.AppViewModel;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
//import com.google.firebase.messaging.FirebaseMessaging;
//import com.unicorn.umart.R;
//import com.unicorn.umart.cart.AddToCartLocal;
//import com.unicorn.umart.cart.AddToCartService;
//import com.unicorn.umart.cart.CartObject;
//import com.unicorn.umart.common.prefutils.SharedPreferenceLiveDataKt;
//import com.unicorn.umart.fragments.FragmentNavDrawer;
//import com.unicorn.umart.interfaces.ModifyCartServiceListener;
//import com.unicorn.umart.manegar.FireBaseLogManager;
//import com.unicorn.umart.pojo.product.TopSearch;
//import com.unicorn.umart.pojo.user.User;
//import com.unicorn.umart.ui.CustomDialog;
//import com.unicorn.umart.util.SharedPreferenceUtil;
//import com.unicorn.umart.viewmodel.UmartViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
//import dagger.android.support.HasSupportFragmentInjector;

public class BaseActivity extends AppCompatActivity implements HasSupportFragmentInjector {
    private static final String LOG_TAG = "BaseActivity";
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    public androidx.appcompat.widget.Toolbar mainToolbar;
    CoordinatorLayout appbarrroot;
    CoordinatorLayout appMainLayout;
    AppBarLayout appbar;
    ViewStub viewStub;
    DrawerLayout drawer;
    Snackbar snackbar;

    private static Dialog progressDialog;
    ActionBarDrawerToggle mToggle;
    private boolean isHandleLogoutRequestPending = false;



    @Inject
    ViewModelProvider.Factory viewModelFactory;
    AppViewModel viewModel;
    private TextView activityTitleTextView;
    private int comiingFrom;

    public View getRootView() {
        return view;
    }

    private View view;

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    private void findViewByIds() {
        this.mainToolbar = ((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar));
        this.appbarrroot = ((CoordinatorLayout) findViewById(R.id.app_bar_root));
        this.appbar = ((AppBarLayout) findViewById(R.id.app_bar));
        this.viewStub = ((ViewStub) findViewById(R.id.base_viewstub));
        this.drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        appMainLayout = (CoordinatorLayout) findViewById(R.id.appMainLayout);
        this.activityTitleTextView = ((TextView) findViewById(R.id.actionbarTitleText));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);
        findViewByIds();
        setSupportActionBar(mainToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


      //  viewModel = ViewModelProviders.of(this, viewModelFactory).get(AppViewModel.class);
        viewModel = new ViewModelProvider(this, viewModelFactory).get(AppViewModel.class);

        mToggle = new ActionBarDrawerToggle(
                this, drawer, mainToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(mToggle);
        mToggle.syncState();


        if (!(this instanceof MainActivity)) {
            this.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        }



        addNavigation();
    }

    private void addNavigation() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.navcontainer, new HomeFragment(), HomeFragment.class.getSimpleName())
                .commit();
    }

    public void closeDrawer() {
        drawer.closeDrawer(GravityCompat.START);
    }

    public void setHomeEnabled(boolean isEnable) {

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        this.mToggle.setDrawerIndicatorEnabled(false);
        Drawable localObject = getResources().getDrawable(R.drawable.back);
        getSupportActionBar().setHomeAsUpIndicator(localObject);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPress();
            }
        });
    }

    private void backPress() {

        if ((this instanceof MainActivity)) {
            if (comiingFrom > 0)
                super.onBackPressed();
            else
                goToHome();
        } else {
            super.onBackPressed();
        }


    }

    private void goToHome() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    public void setCommingFrom(int comiingFrom) {
        this.comiingFrom = comiingFrom;
    }


    public void setTitle(String title) {
       /* if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }*/
        if ((this instanceof MainActivity)) {
            getSupportActionBar().setTitle(title);
        } else {
            activityTitleTextView.setText(title);
        }
    }


    public void hideActionBar() {
        appbarrroot.setVisibility(View.GONE);


    }


    public void setContentView(int paramInt) {

        this.viewStub.setLayoutResource(paramInt);

        if (this.viewStub.getParent() != null) {
            this.view = this.viewStub.inflate();


            return;
        }
        this.viewStub.setVisibility(View.VISIBLE);
        return;
    }

    public ViewStub getViewStub() {
        return viewStub;
    }


    private void launchHomeActivityAfterLogout() {
        Intent localIntent = new Intent(this, MainActivity.class);
        localIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        localIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(localIntent);
    }


    @Override
    public void onBackPressed() {

        if (this.drawer.isDrawerOpen(GravityCompat.START)) {
            this.drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    private boolean isNetworkConnected() {
        NetworkInfo localNetworkInfo = ((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return (localNetworkInfo != null) && (localNetworkInfo.isConnectedOrConnecting());
    }


    public void showProgressDialog() {
        try {
            if ((progressDialog == null) || (!progressDialog.isShowing())) {
                progressDialog = new CustomDialog(BaseActivity.this);
                progressDialog.show();
            }
            return;
        } catch (IllegalArgumentException localIllegalArgumentException) {
            localIllegalArgumentException.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        try {
            if ((progressDialog != null) && (progressDialog.isShowing())) {
                progressDialog.dismiss();
            }
            return;
        } catch (IllegalArgumentException localIllegalArgumentException) {
            localIllegalArgumentException.printStackTrace();
        }
    }

    public void showSnackBar(String paramString) {
        this.snackbar = Snackbar.make(this.appMainLayout, paramString, Snackbar.LENGTH_LONG);
        this.snackbar.show();

    }

}
