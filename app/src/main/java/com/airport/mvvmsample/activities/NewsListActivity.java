package com.airport.mvvmsample.activities;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.airport.mvvmsample.R;
import com.airport.mvvmsample.adapters.NewsAdaptor;
import com.airport.mvvmsample.databinding.ActivityNewsListBinding;
import com.airport.mvvmsample.models.NewsItem;
import com.airport.mvvmsample.models.NewsItemListModel;
import com.airport.mvvmsample.viewmodel.AppViewModel;
import com.airport.mvvmsample.web.Resource;
import com.kwabenaberko.newsapilib.NewsApiClient;
import com.kwabenaberko.newsapilib.models.Article;
import com.kwabenaberko.newsapilib.models.request.EverythingRequest;
import com.kwabenaberko.newsapilib.models.request.SourcesRequest;
import com.kwabenaberko.newsapilib.models.request.TopHeadlinesRequest;
import com.kwabenaberko.newsapilib.models.response.ArticleResponse;
import com.kwabenaberko.newsapilib.models.response.SourcesResponse;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class NewsListActivity extends BaseActivity {


    private AppViewModel viewModel;
    ActivityNewsListBinding binding;
    NewsAdaptor adapter;
    List<Article> newsItemList;
    int page =1;
    int searchpage =1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        binding = DataBindingUtil.bind(getRootView());
        AndroidInjection.inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AppViewModel.class);
        setTitle(getString(R.string.news));
        setHomeEnabled(true);
        newsItemList = new ArrayList<>();


        binding.loadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(binding.edtSearch.getText().toString())){


                    searchpage = searchpage+1;
                    getNewsList(binding.edtSearch.getText().toString());

                }else{
                    page= page+1;
                    getnewsdefault();
                }
            }
        });

        NewsItemClick newsItemClick = new NewsItemClick() {
            @Override
            public void onNewItemCLick(Article newsItem, boolean isClicked) {

                if (newsItem.getUrl().startsWith("https://") || newsItem.getUrl().startsWith("http://")) {


                    Intent newsintent = new Intent(NewsListActivity.this, NewsDetailsActivity.class);
                    newsintent.putExtra("url", newsItem.getUrl());
                    newsintent.putExtra("title", newsItem.getTitle());
                    startActivity(newsintent);
                }

            }
        };


        adapter = new NewsAdaptor(newsItemClick);
        binding.newsList.setAdapter(adapter);
        adapter.replace(newsItemList);


        binding.edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // String action_url = "https://newsapi.org/v2/everything?q="+binding.edtSearch.getText().toString()+"&apiKey=19dc3201557941c39f4d3467264ad40a";
                    if(searchpage ==1){
                        newsItemList.clear();
                    }

                    getNewsList(binding.edtSearch.getText().toString());
                    handled = true;
                }
                return handled;
            }
        });


        //String action_url = "https://newsapi.org/v2/top-headlines?country=in&apiKey=19dc3201557941c39f4d3467264ad40a";

        getnewsdefault();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//
//    }

    public void getNewsList(String serach) {

        binding.loadmore.setVisibility(View.GONE);
        NewsApiClient newsApiClient = new NewsApiClient(getString(R.string.news_api_key));
        showProgressDialog();

// /v2/everything
        newsApiClient.getEverything(
                new EverythingRequest.Builder()
                        .q(serach)
                        .page(searchpage)
                        .build(),
                new NewsApiClient.ArticlesResponseCallback() {
                    @Override
                    public void onSuccess(ArticleResponse response) {
                        hideProgressDialog();
                        Log.e("response", response.getTotalResults()+"");

                        newsItemList.addAll(response.getArticles());

                        adapter.replace(newsItemList);
                        binding.loadmore.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        hideProgressDialog();
                        System.out.println(throwable.getMessage());
                    }
                }
        );

    }

    public void getnewsdefault() {
        binding.loadmore.setVisibility(View.GONE);
        NewsApiClient newsApiClient = new NewsApiClient(getString(R.string.news_api_key));
        showProgressDialog();

//        newsApiClient.getTopHeadlines(new EverythingRequest.Builder().q("texh").build()
//        );

        newsApiClient.getTopHeadlines(
                new TopHeadlinesRequest.Builder()
                        .q("tech")
                        .language("en")
                        .build(),
                new NewsApiClient.ArticlesResponseCallback() {
                    @Override
                    public void onSuccess(ArticleResponse response) {
                        hideProgressDialog();
                        Log.e("response", response.getTotalResults()+"");
                        newsItemList.addAll(response.getArticles());

                        adapter.replace(newsItemList);
                        //binding.loadmore.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        hideProgressDialog();
                        System.out.println(throwable.getMessage());
                    }
                }
        );




    }

    public interface NewsItemClick {
        void onNewItemCLick(Article newsItem, boolean isclick);
    }
}
