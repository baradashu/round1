package com.airport.mvvmsample.web;


import com.airport.mvvmsample.models.NewsItemListModel;
import com.airport.mvvmsample.models.BookItemListModel;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;




public interface WebApi {


   /* @GET("Student/menu")
    LiveData<ApiResponse<MenuList>> getMenu();*/


//    @GET("v2/homepage?profile=details")
//    Call<HomePage> getHomePage();

    @GET
    Call<NewsItemListModel> getNewsList(@Url String url);


    @GET
    Call<BookItemListModel> getBookList(@Url String url);


}
