/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.airport.mvvmsample.binding;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.airport.mvvmsample.R;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
//import com.unicorn.umart.R;
//import com.unicorn.umart.app.UmartApp;
//import com.unicorn.umart.ui.CornersTransformation;
//import com.unicorn.umart.util.MarginDimen;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import static android.content.ContentValues.TAG;

/**
 * Binding adapters that work with a fragment instance.
 */
public class FragmentBindingAdapters {


    @Inject
    public FragmentBindingAdapters() {

    }
//
    @BindingAdapter("imageUrl")
    public static void bindImage(ImageView imageView, String url) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.placeholder_news)
                .error(R.drawable.placeholder_news)
                .into(imageView);
    }

    @BindingAdapter("imgBookUrl")
    public static void bindImgBookUrl(ImageView imageView, String url) {
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(1)
                .cornerRadiusDp(10)

                .oval(false)
                .build();


        Picasso.get()
                .load(url)
                .fit()
                .transform(transformation)
                .placeholder(R.drawable.book_placeholder)
                .error(R.drawable.book_placeholder)
                .into(imageView);
    }



    @BindingAdapter("aimgBookUrl")
    public static void bindAimgBookUrl(ImageView imageView, String url) {
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(1)
                .cornerRadiusDp(10)
                .oval(false)
                .build();


        Picasso.get()
                .load(url)
                .fit()
                .transform(transformation)
                .placeholder(R.drawable.book_placeholder)
                .error(R.drawable.book_placeholder)
                .into(imageView);
    }
//
    @BindingAdapter("ndate")
    public void bindNdate(TextView view, String value) {

        try {
            //Parse the string into a date variable
            Date parsedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH)
                    .parse(value);

//Now reformat it using desired display pattern:
            String displayDate = new SimpleDateFormat("dd-MM-yyyy HH:mm" , Locale.ENGLISH)
                    .format(parsedDate);
            view.setText(displayDate);
        } catch (Exception e) {
            Log.e("bindNdate: ", e.getMessage()+"");
            e.printStackTrace();
            view.setText(value);

        }


    }
//
//    @BindingAdapter("volumn")
//    public void bindVolumn(TextView txview, String value) {
//        if (txview.getVisibility() != View.GONE) {
//            txview.setText(value);
//        }
//    }
//
//    @BindingAdapter("layout_hght")
//    public static void setLayoutHeight(View view, float height) {
//        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
//        layoutParams.height = (int)height;
//        view.setLayoutParams(layoutParams);
//    }
//
//
//
//
//
//    @BindingAdapter({"topMargin", "bottomMargin"})
//    public static void bindMargins(LinearLayout layout, int topMargin, int bottomMargin) {
//
//        LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//
//        localLayoutParams.setMargins(
//                0,
//                (int) MarginDimen.getDimension(UmartApp.getInstance(), topMargin),
//                0,
//                (int) MarginDimen.getDimension(UmartApp.getInstance(), bottomMargin));
//        layout.setLayoutParams(localLayoutParams);
//
//    }
//
//
//    private static boolean checkString(String data) {
//        return (data != null) && (data.equalsIgnoreCase("true"));
//    }
//
//    @BindingAdapter({"topShadow", "bottomShadow", "topCurve", "bottomCurve"})
//    public static void bindPaddings(LinearLayout layout, String topShadow, String bottomShadow, String topCurve, String bottomCurve) {
//
//
//        if (checkString(topShadow) && checkString(bottomShadow)) {
//            layout.setPadding(0, 3, 0, 3);
//            return;
//        }
//        if (checkString(topShadow) && !checkString(bottomShadow)) {
//            layout.setPadding(0, 3, 0, 0);
//            return;
//        }
//        if (!checkString(topShadow) && checkString(bottomShadow)) {
//            layout.setPadding(0, 0, 0, 3);
//            return;
//        }
//        layout.setPadding(0, 0, 0, 0);
//
//
//        if (checkString(topCurve) && checkString(bottomCurve) && checkString(topShadow) && checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_botttom_curve_shadow);
//            return;
//        }
//        if (checkString(topCurve) && !checkString(bottomCurve) && !checkString(topShadow) && !checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_curve);
//            return;
//        }
//
//        if (!checkString(topCurve) && checkString(bottomCurve) && !checkString(topShadow) && !checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.bottom_curve);
//            return;
//        }
//        if (checkString(topCurve) && checkString(bottomCurve) && !checkString(topShadow) && !checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_bottom_curve);
//            return;
//        }
//        if (!checkString(topCurve) && !checkString(bottomCurve) && checkString(topShadow) && !checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_shadow);
//            return;
//        }
//        if (!checkString(topCurve) && !checkString(bottomCurve) && !checkString(topShadow) && checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.bottom_shadow);
//            return;
//        }
//        if (!checkString(topCurve) && !checkString(bottomCurve) && checkString(topShadow) && checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_bottom_shadow);
//            return;
//        }
//        if (checkString(topCurve) && !checkString(bottomCurve) && checkString(topShadow) && !checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_shadow_curve);
//            return;
//        }
//        if (checkString(topCurve) && checkString(bottomCurve) && checkString(topShadow) && !checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_shadow_curve_bottom_curve);
//            return;
//        }
//
//
//        if (!checkString(topCurve) && checkString(bottomCurve) && !checkString(topShadow) && checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.bottom_shadow_curve);
//            return;
//        }
//        if (!checkString(topCurve) && !checkString(bottomCurve) && !checkString(topShadow) && !checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.rectangle);
//            return;
//        }
//        if (!checkString(topCurve) && checkString(bottomCurve) && checkString(topShadow) && checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_shadow_bottom_curve_shadow);
//            return;
//        }
//        if (!checkString(topCurve) && checkString(bottomCurve) && checkString(topShadow) && !checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_shadow_botttom_curve);
//            return;
//        }
//        if (checkString(topCurve) && !checkString(bottomCurve) && !checkString(topShadow) && checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_curve_bottom_shadow);
//            return;
//        }
//        if (checkString(topCurve) && !checkString(bottomCurve) && checkString(topShadow) && checkString(bottomShadow)) {
//            layout.setBackgroundResource(R.drawable.top_curve_shadow_botttom_shadow);
//            return;
//        }
//
//        layout.setBackgroundResource(R.drawable.top_curve_botttom_curve_shadow);
//
//
//    }
//
//
//    @BindingAdapter({"imageSource", "topCurve", "bottomCurve"})
//    public static void bindImages(ImageView imageView, String imageSource, String topCurve, String bottomCurve) {
//        if (imageSource != null) {
//            if (!checkString(topCurve) && !checkString(bottomCurve)) {
//                Picasso.get()
//                        .load(imageSource)
//
//                        .into(imageView);
//            }
//            if (checkString(topCurve) && !checkString(bottomCurve)) {
//                Picasso.get()
//                        .load(imageSource)
//                        .transform(new CornersTransformation(6, 0, CornersTransformation.CornerType.TOP))
//                        .into(imageView);
//            }
//            if (!checkString(topCurve) && checkString(bottomCurve)) {
//                Picasso.get()
//                        .load(imageSource)
//                        .transform(new CornersTransformation(6, 0, CornersTransformation.CornerType.BOTTOM))
//                        .into(imageView);
//            }
//            if (checkString(topCurve) && checkString(bottomCurve)) {
//                Picasso.get()
//                        .load(imageSource)
//                        .transform(new CornersTransformation(6, 0, CornersTransformation.CornerType.ALL))
//                        .into(imageView);
//            }
//        }
//
//    }


}
